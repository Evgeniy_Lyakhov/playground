import express from 'express';
import path from 'path';
import webpack from 'webpack';
import favicon from 'serve-favicon';

import config from './webpack.config.dev';
import handleRequest from './src/bundles/server';

const port = 3000;
const app = express();
const compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: false,
  publicPath: config.output.publicPath,
}));

app.use(require('webpack-hot-middleware')(compiler));

app.use(favicon(path.join(__dirname, 'favicon.ico')));
app.get('*', handleRequest);

app.listen(port, (err) => {
  if (err) {
    console.error(err);
  } else {
    console.info('==> Webpack development server listening on port %s', port);
  }
});
