import React from 'react';
import './main.scss';

const App = ({ children }) => (
  <div>
    <div>Navigation block</div>
    {children}
  </div>
);

App.propTypes = {
  children: React.PropTypes.element.isRequired,
};

export default App;
