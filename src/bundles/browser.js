import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, browserHistory } from 'react-router';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

import mainReducer from '../reducer';
import routes from '../components/routes';

const preloadedState = window.PRELOADED_STATE;
const store = createStore(
  mainReducer,
  preloadedState,
  applyMiddleware(thunk, createLogger())
);

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes()} />
  </Provider>,
  document.getElementById('root')
);
