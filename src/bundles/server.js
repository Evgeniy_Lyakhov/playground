import React from 'react';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

import mainReducer from '../reducer';
import routes from '../components/routes';

const renderPage = (html, state) => (
  `
  <!DOCTYPE html>
  <html>
  <head lang="en">
      <meta charset="UTF-8">
      <title>React start kit</title>
  </head>
  <body>
  <div id="root">${html}</div>
  <script>
      window.__PRELOADED_STATE__ = ${JSON.stringify(state)}
  </script>
  <script src="/dist/bundle.js"></script>
  </body>
  </html>`
);

const requestHandler = (req, res) => {
  match({ routes: routes(), location: req.url }, (error, redirectLocation, renderProps) => {
    if (redirectLocation) {
      res.redirect(301, redirectLocation.pathname + redirectLocation.search);
    } else if (error) {
      res.send(500, error.message);
    } else if (renderProps == null) {
      res.send(404, 'Not found');
    } else {
      const store = createStore(mainReducer, {}, applyMiddleware(thunk, createLogger));
      const html = renderToString(
        <Provider store={store}>
          { <RouterContext {...renderProps} /> }
        </Provider>,
      );
      res.send(renderPage(html, store.getState()));
    }
  });
};

export default requestHandler;
