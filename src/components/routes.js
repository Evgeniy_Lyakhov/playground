import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from '../app';

const routes = () => (
  <Route path="/" component={App}>
    <IndexRoute component={() => (<div>index route</div>)} />
    <Route path="/test" component={() => (<div>Another route</div>)} />
  </Route>
);

export default routes;
